#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile, Image

import random
import time

ev3_img = Image(ImageFile.EV3_ICON)

# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.

# Create your objects here.
ev3 = EV3Brick()

leftMotor = Motor(port=Port.B)
rightMotor = Motor(port=Port.C)
turnMotor = Motor(port=Port.D)

touchSensor = TouchSensor(Port.S1)
leftLineSensor = ColorSensor(Port.S2)
rightLineSensor = ColorSensor(Port.S3)
ultrasonicSensor = UltrasonicSensor(Port.S4)

robot = DriveBase(leftMotor, rightMotor, wheel_diameter=56,
axle_track=114)

def trick_spin_man():
    turnMotor.run_angle(100, 360)

def trick_trex():
    ev3.speaker.play_file('t-rex_roar.wav')

def trick_sideeye():
    ev3.screen.load_image(ImageFile.PINCHED_MIDDLE)
    wait(1000)
    ev3.screen.load_image(ImageFile.PINCHED_LEFT)
    wait(1000)
    ev3.screen.load_image(ImageFile.PINCHED_MIDDLE)
    wait(1000)
    ev3.screen.load_image(ImageFile.PINCHED_RIGHT)
    wait(1000)
    ev3.screen.load_image(ImageFile.PINCHED_MIDDLE)
    wait(1000)
    ev3.screen.load_image(ImageFile.DIZZY)
    wait(1000)
    ev3.screen.load_image(ImageFile.KNOCKED_OUT)
    wait(1000)
    ev3.screen.clear()

def trick_spin360():
    leftMotor.run_angle(500,360*5)

RED = 50
GREEN = 50
BLUE = 50

BLACK = 9
WHITE = 85
threshold = 50

SPEED = 200
PROPORTIONAL_GAIN = 1.2
turn_rate = 0 #minus er venstre, pluss er høyre

last_time = time.time()
antall_stopp = 0


while True:

   
    #deviation = leftLineSensor.reflection() - threshold
    turn_rate = leftLineSensor.reflection() - rightLineSensor.reflection()

    turn_rate= PROPORTIONAL_GAIN * turn_rate
    robot.drive(SPEED, turn_rate)
    wait(10)

    if ultrasonicSensor.distance() < 100:
        robot.stop()
        ev3.speaker.play_file('cheering.wav')
        break

    current_time = time.time()

    if (current_time - last_time >= 10):
        if antall_stopp < 4:
            random_tall = random.randint(0,3) 
            robot.stop()
        

            if (random_tall == 0):
                trick_sideeye()
            elif (random_tall == 1):
                trick_spin360()
            elif (random_tall == 2):
                trick_spin_man()
            elif (random_tall == 3):
                trick_trex()

        antall_stopp += 1
        last_time = time.time()
    
   