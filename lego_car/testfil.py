#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile, Image

import random
import time

ev3_img = Image(ImageFile.EV3_ICON)

# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.

# Create your objects here.
ev3 = EV3Brick()

leftMotor = Motor(port=Port.B)
rightMotor = Motor(port=Port.C)
turnMotor = Motor(port=Port.D)

#touchSensor = TouchSensor(Port.S1)
leftLineSensor = ColorSensor(Port.S2)
rightLineSensor = ColorSensor(Port.S3)
#ultrasonicSensor = UltrasonicSensor(Port.S4)

robot = DriveBase(leftMotor, rightMotor, wheel_diameter=56,
axle_track=114)

def hev_spin():
    turnMotor.run_angle(100, 100)

def senk_spin():
    turnMotor.run_angle(100, -100)




hev_spin()
