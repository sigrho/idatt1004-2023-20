#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile


# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.


# Create your objects here.
ev3 = EV3Brick()


rightMotor = Motor(port=Port.B)
leftMotor = Motor(port=Port.C)

touchSensor = TouchSensor(Port.S1)
ultrasonicSensor = UltrasonicSensor(Port.S4)

robot = DriveBase(leftMotor, rightMotor, wheel_diameter=56,
axle_track=114)

def kjorFrem():
    robot.drive(100,0)

def svinghoyre():
    robot.turn(90)  

while not touchSensor.pressed():
    continue

ev3.speaker.say('Exercise 2')
wait(10)
    

while not touchSensor.pressed():
    kjorFrem()
    if ultrasonicSensor.distance() < 40:
        svinghoyre()
        kjorFrem()
 
robot.stop()
ev3.speaker.say('Exercise done') 





